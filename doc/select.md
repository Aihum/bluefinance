# Select

**class** - select

**placeholder** - select placeholder

**options** - select options

**submitOnChange**: - if true, changes in the elements value will trigger /send
request. (by default = false)

**Normal state**

![alt text](./img/select-closed.png)

**Opened state**

![alt text](./img/select-opened.png)

**JSON Example**

```javascript
{
    "id": "mob_type",
    "row": "3",
    "class": "select",
    "title": "Typo de contrato movil",
    "placeholder": "Test",
    "required": true,
    "submitOnChange": false,
    "errorMsg": "Error",
    "value": "1",
    "options": [
        { "value": "1", "title": "Abano de servicio post-pago" },
        { "value": "2", "title": "Value2", 'visibility': 'visible' }
        ]
}

```
