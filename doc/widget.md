# Widget

**class** = widget

**style** - object with css styles

**extra** = { "widgetUrl": "{{widgetUrl}}" }

**extra.allowFullScreen** = true|false (default value)

**extra.nemId** = {"parameters": "{{parameters}}"} 

parameters = normalized, and signed init parameters that wiil be send to nemId widget after "SendParameters" command will be recieved.

[nemId documentation](https://www.nets.eu/dk-da/kundeservice/nemid-tjenesteudbyder/The-NemID-service-provider-package/Pages/documentation.aspx)

**JSON Example**

```javascript
{
    "id": "mymoid",
    "class": "widget",
    "style": {"height":"320px"},
    "extra": { "widgetUrl": "{{widgetUrl}}", "allowFullScreen": false }
}
```

```javascript
<iframe src={widgetUrl} allowfullscreen={allowFullScreen} style={style}/>
```
If Iframe use postMessage, all events will be sent to MPO via /send request

**/send request body Example**

```javascript
 "body": {
        "formId": "card_validation",
        "buttonId": "mymoid",
        "data": {
            "mymoid": {"result":"ok"}
        }
    }
```
If iframe use redirect url, internal redirect url can be used. On page load, script will send back search parameters to parent window, and front-end app will send to MPO via /send request.

**Internal redirect url**

https://test.nexu.es/api/1/iframe


**Redirrect url with search params Example**

https://test.nexu.es/api/1/iframe?result=ok

**/send request body Example**

```javascript
 "body": {
        "formId": "card_validation",
        "buttonId": "mymoid",
        "data": {
            "result": "ok"
        }
    }
```

**Mymoid widgetUrl example**

 
url structure according to [documentation](https://apidocumentation.mymoid.com/#basic-payment-form) : {mymoidServerUrl}/tpv/?p={shortCode}&urlok={URL_OK}&urlko={URL_KO}

var widgetUrl = https://sandbox.mymoid.com/tpv/?p=3HGYGR&urlok=https%3A%2F%2Ftest.nexu.es%2Fapi%2F1%2Fiframe%3Fresult%3Dok&urlko=https%3A%2F%2Ftest.nexu.es%2Fapi%2F1%2Fiframe%3Fresult%3Derror

URL_OK, URL_KO contains "result" custom parameter that will be  analized in MPO.
Both url's are urlencoded with https://www.urlencoder.org
