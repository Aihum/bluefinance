# Phone

**class** = phone

**value** = {countryCode: “380”, number :“67000011”}

**options** - countryCode options

**extra** - { placeholder : {countryCode: '', number: ''}}

**regexp** - regular expression for validation the value

**Normal state**

![alt text](./img/phone.png)

**Error state**

![alt text](./img/phone-error.png)

**Normal state with value**

![alt text](./img/phone-with-no-error.png)

**JSON Example**

```javascript
  {
    "id": "mobile",
    "class": "phone",
    "title": "[[MobilePhoneNumber]]",
    "required": false,
    "errorMsg": "Please check phone number",
    "options": [{ "title": "+358", "value": "358" }],
    "regexp": "^[0-9]{9,17}$",
    "extra": {
      "placeholder": {
        "countryCode": "[[code]]",
        "number": "[[number]]"
      }
    }
  }
```
