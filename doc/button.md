# Button
**class** = button

**type** - submit|redirect|link|secondary

**style** - object with css styles

**extra** - object with extra parameters

```javascript
"extra": {"url": "https://corezoid.com"} // redirect url for type=redirect
```

**type:"submit | redirect", visibility: "visible"**

![alt text](./img/button-visible.png)

**type:"submit | redirect", visibility: "disabled"**

![alt text](./img/button-disabled.png)

**type:"secondary"**

![alt text](./img/button-secondary.png)

**type:"link"**

![alt text](./img/button-link.png)


**JSON Example**

```javascript
{
    "id": "button",
    "class": "button",
    "type": "submit",
    "visibility": "visible"
}

```
