
# General info

**Page** - web page representation.

**Grid** - layout options.

**Header** - static html that will be placed on the top of the page

**Form** - set of fields that are grouped in sections

**Section**- set of items grouped visually

**Item** - element of the web interface. It’s design and behavior  implemented in a source code of front-end app.

# Common items attributes

**id** - unique item identifier. The same id will be send back with the value after submit form

**class** - type of item. Button, edit, etc

**value** - initial item value (string or object)

**visibility** - visible | disabled | hidden (default value = visible)

**required** - true | false (default value = false). If required, Form submit button will be disabled until element will have correct value

**helpMsg** - message that will be displayed under element when there is no error

**errorMsg** - message that will be displayed under element in case validation fail

**row** - items with the same rowId will by aligned in one line.


# Page

Page consist of **header**, **grid** and **forms**.

Available grid types : **one_coumn**, **two_column**.

**one_coumn grid**
![alt text](./img/grid-one-column.png)

**two_coumn grid**
![alt text](./img/grid-two-column.png)

Available header classes :

**default**
![alt text](./img/header-default.png)
**steps**. Number of steps and active step is set in
**extra** field.
![alt text](./img/header-steps.png)


**one_column grid with default header**
```javascript
{
  "grid": {
     "type": "one_column",
     "header": {
       "class": "default"
   },
  "components": {
     "center": []
   }
 },
  "forms": []
}
```

**two_column grid with steps header**
```javascript
{
 "grid": {
  "type": " two_column",
  "header": {
     "class": "steps",
     "extra": {
        "steps": [
          "Information[br]personal",
          "Direction",
          "Information[br]economica",
          "Indetification",
          "Identification[br]banacaria"
        ],
        "active": 1
      }
   }

 "components": {
      "left": [],
      "right": []
 },
 "forms": []

}
```
# Example

**log_in page. one column grid**
```javascript
{
 "grid": {
   "type": "one_column",
   "header": {
     "class": "steps",
     "extra": {
       "steps": [
         "Information[br]personal",
         "Direction",
         "Information[br]economica",
         "Indetification",
         "Identification[br]banacaria"
       ],
       "active": 1,
       "additionalInfo": "[b]?Necesitas ayuda?[/b] Liamonos al 9XXXXXXXXX"
     }
   },
   "components": {
     "center": ["log_in"]
   }
 },
 "forms": [
   {
     "id": "log_in",
     "type": "body",
     "title": "Identificate para solicitar tu prestamo",
     "sections": [
       {
         "content": [
           {
             "class": "label",
             "value": "En 10 minutos tu dinero en tu cuenta"
           },
           {
             "id": "DNINIE",
             "class": "edit",
             "title": "DNI/NIE",
             "placeholder": "placeholder",
             "required": true,
             "errorMsg": "Error",
             "regex": ""
           },
           {
             "id": "button",
             "class": "button",
             "type": "submit",
             "visibility": "disabled",
             "title": "Continuar"
           }
         ]
       }
     ]
   }
 ]
}
```

**info_personal page. two column grid**

``` javascript
{
 "grid": {
   "type": "two_column",
   "header": {
     "class": "headerWithSteps",
     "extra": {
       "steps": [
         "Information[br]personal",
         "Direction",
         "Information[br]economica",
         "Indetification",
         "Identification[br]banacaria"
       ],
       "active": 1,
       "additionalInfo": "[b]?Necesitas ayuda?[/b] Liamonos al 9XXXXXXXXX"
     }
   },
   "components": {
     "left": ["info_personal"],
     "right": ["loan_calculator"]
   }
 },
 "forms": [
   {
     "id": "info_personal",
     "type": "body",
     "title": "Informacion personal",
     "sections": [
       {
         "content": [
           {
             "id": "name",
             "row": "1",
             "class": "edit",
             "title": "Name",
             "errorMsg": "Error",
             "required": true
           },
           {
             "id": "surname",
             "row": "1",
             "class": "edit",
             "title": "Surname",
             "errorMsg": "Error",
             "required": true
           },
           {
             "id": "lastName",
             "row": "1",
             "class": "edit",
             "title": "Last name",
             "errorMsg": "Error",
             "required": true
           },
           {
             "id": "DNINIE",
             "row": "2",
             "class": "edit",
             "title": "DNI/NIE",
             "regex": "",
             "required": true,
             "errorMsg": "Error"
           },
           {
             "id": "dob",
             "row": "2",
             "class": "calendar",
             "title": "Fecha de nacimiento",
             "errorMsg": "Error",
             "required": true
           },
           {
             "id": "mob",
             "row": "3",
             "type": "phone",
             "class": "edit",
             "title": "Telefono Movil",
             "required": true,
             "errorMsg": "Error"
           },
           {
             "id": "mob_type",
             "row": "3",
             "class": "select",
             "title": "Typo de contrato movil",
             "required": true,
             "errorMsg": "Error",
             "value": "1",
             "options": [
               { "value": "1", "title": "Abano de servicio post-pago" },
               { "value": "2", "title": "Value2" }
             ]
           },
           {
             "id": "email",
             "class": "edit",
             "title": "Email",
             "required": true,
             "errorMsg": "Error",
             "type": "email"
           },
           {
             "id": "iban",
             "class": "edit",
             "title": "Numero de cluenta banacaria (IBAN)",
             "required": true,
             "errorMsg": "Error"
           },
           {
             "id": "consent_1",
             "class": "check",
             "title": "Autorizo a blue Finance.SA y las Empresas del Grupo a que traten mis datos personales con fines publicitarios y de marketing ([url=https://nexu.es] as informacion[/url])",
             "required": true,
             "errorMsg": "Error"
           },
           {
             "id": "loan_params",
             "class": "calculator",
             "visibility": "hidden"
           },
           {
             "id": "button",
             "class": "button",
             "type": "submit",
             "visibility": "disabled",
             "title": "Continuar"
           }
         ]
       }
     ]
   },
   {
     "id": "loan_calculator",
     "type": "body",
     "sections": [
       { "content": [{ "id": "loan_params", "class": "calculator" }] }
     ]
   }
 ]
}
```
