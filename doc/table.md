# Table

**class** = list

**value** = array of rows, where each row is an array of cells.

supports [bbcode](./bbcode.md)

![alt text](./img/table.png)


**JSON Example**

```javascript
{
   "id":"table",
   "class":"table",
   "value":[
      [
         "[b]Due date[/b]",
         "[b]Paid out on[/b]",
         "[b]Amount &euro;[/b]"
      ],
      [
         "29.07.2020",
         "10.06.2020",
         "100.00 &euro;"
      ]
   ]
}
```
