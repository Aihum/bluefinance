# Form

Form has a title and sections.

Every section has content. Content consists of items;

**Structure**
![alt text](./img/form.png)

**id** = form id

**visibility** = form visibility

**title** = form title with [bbcode](./bbcode.md)


**sectoins** array of form sections. Each section has header, content.

**id** = section id

**visibility** = section visibility

**type** = body|block (default=body). By default sections content has transparent background, as a result background ingerits from the body of the page (type=body)When type=block, section content has white background.

**header** = array of section header items

**content** = array of section content items

**Example when section type=body**

![alt text](./img/form-body.png)

**Example when section type=block**

![alt text](./img/form-block.png)


**Example when section type=modal**

![alt text](./img/form-modal.png)

**Error state**

![alt text](./img/form-error.png)


**JSON Example:**
```javascript
{
     "id": "log_in",
     "type": "body",
     "title": "Identificate para solicitar tu prestamo",
     "sections": [
       {
         "id": "sectionOne",
         "visibility": "visible",
         "type": "body",
         "header": [],
         "content": [
           {
             "class": "label",
             "value": "En 10 minutos tu dinero en tu cuenta"
           },
           {
             "id": "DNINIE",
             "class": "edit",
             "title": "DNI/NIE",
             "required": true,
             "errorMsg": "Error",
             "regex": ""
           },
           {
             "id": "button",
             "class": "button",
             "type": "submit",
             "visibility": "disabled",
             "title": "Continuar"
           }
         ]
       }
     ]
   }

```
