# Label

**class** - label

**align** - left | center | right (default value = left)

**value** - text with [bbcode](./bbcode.md)

**style** - object with css styles

**align:"left"**

![alt text](./img/label-align-left.png)

**align:"center"**

![alt text](./img/label-align-center.png)

**align:"right"**

![alt text](./img/label-align-right.png)

**style:{ "fontSize": "14px" }**

![alt text](./img/label-with-style.png)

**JSON Example**

```javascript
{
    "id": "11",
    "row": "3",
    "class": "label",
    "value": "En 10 minutos tu dinero en tu cuenta",
    "align": "center",
}
```
