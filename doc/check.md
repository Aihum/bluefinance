# Check

**class** - check

**title** - text with [bbcode](./bbcode.md)

**submitOnChange**: - if true, changes in the elements value will trigger /send
request. (by default = false)

**Not checked state**

![alt text](./img/check-not-selected.png)

**Checked state**

![alt text](./img/check-selected.png)

**Error state**

![alt text](./img/check-error.png)

**JSON Example**

```javascript
    {
    "id": "consent_1",
    "class": "check",
    "title": "Autorizo a blue Finance.SA y las Empresas del Grupo a que traten mis datos personales con fines publicitarios y de marketing ([url=https://nexu.es] as informacion[/url])",
    "required": true,
    "submitOnChange": false,
    "errorMsg": "Error"
    }
```
