# Calendar
**class** = calendar

**value** = {d: “01”,  m :“12”,  y: “2000”}

**extra** = date constraints. default value  = { min: { y: -100, m: 0, d: 0 }, max: { y: 0, m: 0, d: 0 } }

**Not selected state**

![alt text](./img/calendar-with-no-value.png)

**Selected state**

![alt text](./img/calendar-with-value.png)

**Error state**

![alt text](./img/calendar-error.png)

**JSON Example**

```javascript
{
    "id": "dob",
    "row": "2",
    "class": "calendar",
    "title": "Fecha de nacimiento",
    "errorMsg": "Error",
    "required": true
}
```
