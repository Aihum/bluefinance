# Bbcode

Label value, Check title,  Form title, Form errorMsg can contain Bbcode. 

Bbcode - it is a string with pseudo code, that will be converted into HTML.

**Supported tags**

| bbcode        | HTML           |
| ------------- |:-------------:|
| [b]Bold text[/b]      | <b>Bold text</b> |
| [i]Italic text[/i]      | <i>Italic text</i>      |
| [u]Underline text[/u]      | <u>Underline text</u>      |
| [ul][\*]Unordered list[\/*][/ul]      | <ul><li>Undordered list</li></ul>      |   |
| [iurl=/log_in]Internal link[/iurl] | <a href="/log_in">Internal link</a>      |
| [url=https://nexu.es] External link[/url] | <a href="https://nexu.es" target="_blank" rel="noopener noreferrer">External link</a>      |
| [div]New line[/div]  | <div>New line</div>      |
| [br]      | <br/>      |
| [color=red]Text color[/color]  | <span style="color:red">Text color</span>      |
| [size=12]Font size[/size]  | <span style="font-size:12px">Font size</span>      |
| [bg=green]Text back ground[/bg]  | <span style="background-color:green">Text back ground</span>      |
