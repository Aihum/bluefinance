# Edit

**class** = edit

**type** - text | int | float | phone | email (default value = text)

**extra** - {length : 2} (no default value)

**regexp** - regular expression for validation the value if item is required

**placeholder** - input placeholder

**submitOnChange**: - if true, changes in the elements value will trigger /send
request. (by default = false)

**Normal state**

![alt text](./img/edit-with-no-value.png)

**Normal state with helpMsg**

![alt text](./img/edit-help-msg.png)

**Error state**

![alt text](./img/edit-error.png)

**JSON Example**

```javascript
{
    "id": "DNINIE",
    "class": "edit",
    "title": "DNI/NIE",
    "palceholder": "placeholder",
    "regex": "",
    "visibility": "visible",
    "required": true,
    "submitOnChange": false,
    "errorMsg": "Error"
}

```
