# Radio

**class** - radio

**options** - radio options

**align** - verical|horizontal (by default = vartical)

**submitOnChange**: - if true, changes in the elements value will trigger /send
request to the server. (by default = false)

**Vertical**

![alt text](./img/radio-vertical.png)

**Horizontal**

![alt text](./img/radio-horizontal.png)

**JSON Example**

```javascript
{
    "id": "mob_type",
    "class": "radio",
    "title": "Typo de contrato movil",
    "required": true,
    "submitOnChange": false,
    "errorMsg": "Error",
    "value": "1",
    "options": [
        { "value": "1", "title": "Abano de servicio post-pago" },
        { "value": "2", "title": "Value2", 'visibility': 'visible' }
        ]
}

```
