# Divider

**class** = divider


![alt text](./img/divider.png)


**JSON Example**

```javascript
{
    "id": "divider",
    "class": "divider"
}
```
