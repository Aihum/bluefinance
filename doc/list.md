# List

**class** = list

**extra** = list settings = { header: "", content: [], footer: "" }

supports [bbcode](./bbcode.md)

![alt text](./img/list.png)


**JSON Example**

```javascript
{
    "id": "list",
    "class": "list",
    "extra": {
        "header": "Para solicitar tu credito necessitas:",
        "content": [
            { "value" : "Residir en Espana" },
            { "value" : "Dispones de DNI/NIE espanol" },
            { "value" : "Tener mas de 20 anos" },
            { "value" : "Set titular una Cuenta banacaris en Espana" }
        ],
        "footer": "[b]Necesitas ayuda?[/b] Liamonos al 9XXXXXXXXXX"
    }
}
```
